package com.doomshade.compass.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.doomshade.compass.Compass;
import com.doomshade.compass.Menu;

public class MenuEvents implements Listener {
	private FileConfiguration excludeLoader;
	public static final String KOMPAS = ChatColor.DARK_GREEN + "�lKompas";

	private List<String> translateColors(List<String> lore) {
		List<String> returnLore = new ArrayList<String>();
		for (int i = 0; i < lore.size(); i++) {
			returnLore.add(ChatColor.translateAlternateColorCodes('&', lore.get(i)));
		}
		return returnLore;
	}

	private static boolean isCompass(ItemStack item) {
		if (item == null || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName()
				|| !item.getItemMeta().getDisplayName().equalsIgnoreCase(KOMPAS)) {
			return false;
		}
		return true;
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if (isCompass(e.getItemDrop().getItemStack())) {
			e.getItemDrop().remove();
		}
	}

	@EventHandler
	public void onSlotClick(InventoryClickEvent e) {

		if (e.getClickedInventory() == null || e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta()
				|| !e.getCurrentItem().getItemMeta().hasDisplayName()) {
			return;
		}

		excludeLoader = Compass.getInstance().getLoader();

		String invName = e.getClickedInventory().getTitle();

		String jmenoNpc = e.getCurrentItem().getItemMeta().getDisplayName();

		Player hrac = (Player) e.getWhoClicked();

		List<String> exclude = translateColors(excludeLoader.getStringList("exclude"));
		if (!exclude.contains(jmenoNpc)) {
			setCompassLocation(invName, jmenoNpc, hrac);
		}
	}

	// Nastavi lokaci kompasu hrace
	private void setCompassLocation(String invName, String jmenoNpc, Player hrac) {
		if (!Menu.MENUS.containsKey(invName)) {
			return;
		}
		Menu menu = Menu.MENUS.get(invName);
		Location compassLocation = menu.getCompassLocation(ChatColor.stripColor(jmenoNpc), hrac);
		if (compassLocation == null)
			return;
		ItemStack kompas = new ItemStack(Material.COMPASS);
		ItemMeta meta = kompas.getItemMeta();
		meta.setDisplayName(KOMPAS);
		meta.setLore(Arrays.asList(ChatColor.BLUE + "�oZavede te k " + jmenoNpc));
		kompas.setItemMeta(meta);
		for (ItemStack possibleCompass : hrac.getInventory().getContents()) {
			if (possibleCompass == null || !possibleCompass.hasItemMeta()
					|| !possibleCompass.getItemMeta().hasDisplayName()
					|| !possibleCompass.getItemMeta().getDisplayName().equalsIgnoreCase(KOMPAS)) {
				continue;
			}

			hrac.getInventory().remove(possibleCompass);
		}
		hrac.getInventory().remove(Material.COMPASS);
		hrac.getInventory().addItem(kompas);
		hrac.setCompassTarget(compassLocation);
		hrac.sendMessage(ChatColor.DARK_AQUA + "Kompas nastaven na lokaci NPC " + jmenoNpc + "("
				+ compassLocation.getWorld() + ", " + compassLocation.getBlockX() + ", " + compassLocation.getBlockY()
				+ ", " + compassLocation.getBlockZ() + ")");
	}
}
