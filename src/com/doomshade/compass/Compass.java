package com.doomshade.compass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.doomshade.compass.listener.MenuEvents;

public class Compass extends JavaPlugin {
	// File nejlepe ve main tride, jak jsem to udelal ja, a incicializujes v
	// onEnable()
	private File chestCommandsFile, excludeNpcs;
	private File[] menu;

	// FileConfiguration ti umozni pracovat se soubory - napr.
	// loader.getString("shit") ti bude hledat v nejakem souboru 'shit: 'nejaky
	// string'' a vrati ti to 'nejaky string'
	private FileConfiguration loader;

	// Toto je dulezity, nemuzes vytvorit objekt main tridy (objekt vytvoris pomoci
	// new Jmeno_Tridy()). V teto tride mas mnoho dulezitych metod, proto se vytvari
	// instance teto tridy
	private static Compass instance;

	// Tohle zajistuje, ze loader bude inicializovan, nesmi to byt static. TLDR,
	// nedelej v main tride static atributy, ktere by mohly byt null, jako je napr.
	// u me ten loader
	public FileConfiguration getLoader() {
		return loader;
	}

	// To same
	public File[] getMenu() {
		return menu;
	}

	// Toto vytvory takovy template pro soubor - jakmile vytvoris soubor, tak se tam
	// vytvori nove pole stringu. V souboru to bude vypadat asi takto: 'exclude:[]'
	private Map<String, Object> defaults() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("exclude", new ArrayList<String>());
		return map;
	}

	@Override
	public void onEnable() {
		// Takhle jsem nasel soubory z jinych pluginu - tady je to z ChestCommands
		chestCommandsFile = new File("plugins/ChestCommands/menu");

		// Vytvori slozku tveho pluginu ve slozce plugins
		if (!getDataFolder().isDirectory()) {
			getDataFolder().mkdirs();
		}

		// Definice noveho souboru. Pokud potrebujes vytvorit novy soubor ve slozce
		// pluginu, tak to bude VZDYCKY zacinat s 'getDataFolder() + File.separator +
		// "jmeno_souboru.yml"
		excludeNpcs = new File(getDataFolder() + File.separator + "exclude.yml");

		// Takhle bude vypadat KAZDE vytvareni souboru - if(!soubor.exists())
		// {soubor.createNewFile()}, pokud soubor neexistuje, vytvori se
		if (!excludeNpcs.exists()) {
			try {
				excludeNpcs.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Inicializace nastaveni souboru
		loader = YamlConfiguration.loadConfiguration(excludeNpcs);

		// Tady pripisuju do souboru vyse zminene pole stringu exclude: [] z metody
		// defaults()
		loader.addDefaults(defaults());
		loader.options().copyDefaults(true);

		// Ukladani souboru bude bzdy vypadat takto
		try {
			loader.save(excludeNpcs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Toto jsou vsechny soubory v plugins/ChestCommands/mesta (tahle directory
		// existuje jen u mne, mesta je podslozkou pluginu ChestCommands)
		menu = chestCommandsFile.listFiles();

		// Tahle trida "provozuje" chod vsech pluginu, vcetne tveho, a dokaze
		// registerovat listenery
		PluginManager pm = Bukkit.getPluginManager();

		// Tohle musis napsat, aby ti fungoval listener. O listeneru se dozvis v
		// MenuEvents
		pm.registerEvents(new MenuEvents(), this);

		// Nastavi instanci teto tridy (instance neni objekt), je jedno, jestli je to na
		// zacatku onEnable() nebo tady, hlavni je, ze je to nekde. Tuhle metodu musis
		// rucne vytvorit, viz nize
		setInstance(this);
		Menu.init();
	}

	@Override
	public void onDisable() {
		try {
			loader.save(excludeNpcs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Compass getInstance() {
		return instance;
	}

	private static void setInstance(Compass instance) {
		Compass.instance = instance;
	}

}
