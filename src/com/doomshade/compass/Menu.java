package com.doomshade.compass;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Menu {
	/**
	 * String = configName
	 */
	public static Map<String, Menu> MENUS = new HashMap<>();

	// JmenoNPC + lokaceNPC
	private Map<String, Location> lokace;
	private String invName;

	private Menu(FileConfiguration loader) {
		if (!loader.isString("menu-settings.name")) {
			return;
		}

		this.lokace = new HashMap<>();
		this.invName = ChatColor.translateAlternateColorCodes('&', loader.getString("menu-settings.name"));
		for (String key : loader.getKeys(false)) {
			ConfigurationSection section = loader.getConfigurationSection(key);
			if (!section.isString("NAME") || section.getString("NAME") == null
					|| section.getString("NAME").equalsIgnoreCase("")) {
				continue;
			}
			String jmenoNpc = ChatColor
					.stripColor(ChatColor.translateAlternateColorCodes('&', section.getString("NAME")));
			double x, y, z;
			x = y = z = 0;
			for (String loreKey : section.getStringList("LORE")) {
				if (!loreKey.contains("X:")) {
					continue;
				}
				try {
					for (String sp : loreKey.split(" ")) {
						if (sp.contains("X:")) {
							x = Integer.parseInt(sp.replaceAll("[^-?0-9]+", ""));
						} else if (sp.contains("Y:")) {
							y = Integer.parseInt(sp.replaceAll("[^-?0-9]+", ""));
						} else if (sp.contains("Z:")) {
							z = Integer.parseInt(sp.replaceAll("[^-?0-9]+", ""));
						}
					}
				} catch (NumberFormatException e) {
					break;
				}
				this.lokace.put(jmenoNpc, new Location(null, x, y, z));
				break;
			}

		}
	}

	public Map<String, Location> getLocations() {
		return lokace;
	}

	public Location getCompassLocation(String npcName, Player hrac) {
		if (!this.lokace.containsKey(npcName)) {
			return null;
		}
		Location lokace = this.lokace.get(npcName);
		lokace.setWorld(hrac.getWorld());
		return lokace;
	}

	public String getName() {
		return invName;
	}

	public static void init() {
		MENUS = new HashMap<>();
		File[] menu = Compass.getInstance().getMenu();
		Menu _menu;
		for (File file : menu) {
			_menu = new Menu(YamlConfiguration.loadConfiguration(file));
			MENUS.put(_menu.getName(), _menu);
		}
	}
}
